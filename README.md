# CdM-8-Cheat-Sheet

This is a cheat sheet developed by Tyler for the CdM-8 processor built by Michael Walters and Alex Shafarenko at the University of Hertfordshire

You can view the website **[here](https://nexusnovaz.gitlab.io/)**

This book will be updated as we find out more information on the instructions.

# Contributing

Anyone can submit a pull request to modify a file or create their own.


This collection of tutorials is written using [Markdown](https://www.markdownguide.org/)
and then compiled using [mdBook](https://github.com/rust-lang-nursery/mdBook)
you can download it from their [releases](https://github.com/rust-lang-nursery/mdBook/releases)
page.

To run the book you can use
```
mdbook serve
```
It will start a webserver at http://localhost:3000/ which you can visit using your browser to see the rendered website. As you edit you can save the files and the webpage will auto reload showing changes instantly.
If you find an error fork the project and fix it yourself or let me know on discord: Nexus Novaz#0862 or using the GitLab issues page.

I edit using Atom with the [Markdown Table Editor](https://atom.io/packages/markdown-table-editor) plugin. I highly recommend using it if you want to add to the tables.

## Important stuff included in this repository are:
- `.gitlab-ci.yml`, set up to automatically build the site and release it on GitLab Pages
- `book.toml`, just some basic configuration settings for mdBook
- `.gitignore`, stops the output from mdBook being committed to the Git repository

## The `src` directory
This is where the actual Markdown files go.
`SUMMARY.md` is the indexing file; it specifies all the Markdown files that are part of the tutorial. When everything is compiled into a website this file is read to decide what order to put the pages in.

## Licensing
<a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png" /></a><br /><span xmlns:dct="http://purl.org/dc/terms/" property="dct:title">CdM-8 Cheat Sheet</span> by <a xmlns:cc="http://creativecommons.org/ns#" href="https://gitlab.com/NexusNovaz/cdm-8-cheat-sheet" property="cc:attributionName" rel="cc:attributionURL">Tyler Staples</a> is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/">Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License</a>.
