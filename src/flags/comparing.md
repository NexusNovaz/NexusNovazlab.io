# Flags

| Name         | conditional mnemonic | PS flags | C V Z N | Logical Expression | Conventional Interpretation |
| ------------ | -------------------- | -------- | ------- | ------------------ | --------------------------- |
| Equal        | eq                   |          |         |                    |                             |
| Not Equal    | ne                   |          |         |                    |                             |
| Less Than    | lt                   |          |         |                    |                             |
| Zero         | z                    |          |         |                    |                             |
| Greater Than | gt                   |          |         |                    |                             |
| Minus        | mi                   |          |         |                    |                             |
| Carry Clear  | cc                   |          |         |                    |                             |
| Carry Set    | cs                   |          |         |                    |                             |
| Not Zero     | nz                   |          |         |                    |                             |
| plus         | pl                   |          |         |                    |                             |
| Overflow Set | vs                   |          |         |                    |                             |
|              |                      |          |         |                    |                             |
