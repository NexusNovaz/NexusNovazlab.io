# Compiler Directive Instructions

| Instruction | Description                                                         | Usage                                                    | Example                                              |
|:-----------:| ------------------------------------------------------------------- | -------------------------------------------------------- | ---------------------------------------------------- |
|    asect    | Absolute Section. Tells the program what memory address to start at | <code><span class="green">asect</span> \<address></code> | <code><span class="green">asect</span> 0x00</code>   |
|    rsect    |                                                                     | <code><span class="green">rsect</span> EDIT </code>      | <code><span class="green">rsect</span> EDIT </code>  |
|     end     | Tells the program where to stop reading memory                      | <code><span class="green">end</span></code>              | <code><span class="green">end</span></code>          |
|     dc      | Declares a constant                                                 | <code><span class="green">dc</span> \<value></code>      | <code><span class="green">dc</span> 0x3A</code>      |
|     ds      | Declares a section for storage in bytes                             | <code><span class="green">ds</span> \<bytes></code>      | <code><span class="green">ds</span> 2</code>         |
|     ext     |                                                                     | <code><span class="green">ext</span> EDIT </code>        | <code><span class="green">ext</span> EDIT </code>    |
|   tplate    |                                                                     | <code><span class="green">tplate</span> EDIT </code>     | <code><span class="green">tplate</span> EDIT </code> |
|     set     |                                                                     | <code><span class="green">set</span> EDIT </code>        | <code><span class="green">set</span> EDIT </code>    |
|    page     |                                                                     | <code><span class="green">page</span> EDIT </code>       | <code><span class="green">page</span> EDIT </code>   |
