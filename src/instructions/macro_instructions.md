<h1> Macro Instructions </h1>
<link rel="stylesheet" href="/book/highlight.css">
<link rel="stylesheet" href="/book/custom.css">
<script src="/book/highlight.js"></script>
<script>hljs.initHighlightingOnLoad();</script>


| Instruction | Description               | Usage                                                     | Example                                                   |
|:-----------:| ------------------------- | --------------------------------------------------------- | --------------------------------------------------------- |
|     run     |                           | <code><span class="purple">run</span> EDIT </code>        | <code><span class="purple">run</span> EDIT </code>        |
|    else     |                           | <code><span class="purple">else</span> EDIT </code>       | <code><span class="purple">else</span> EDIT </code>       |
|     if      | Start of an if statement  | <code><span class="purple">if</span> EDIT </code>         | <code><span class="purple">if</span> EDIT </code>         |
|     fi      | End of an if statement    | <code><span class="purple">fi</span> EDIT </code>         | <code><span class="purple">fi</span> EDIT </code>         |
|     is      |                           | <code><span class="purple">is</span> EDIT </code>         | <code><span class="purple">is</span> EDIT </code>         |
|     gt      | Greater than              | <code><span class="purple">gt</span> EDIT </code>         | <code><span class="purple">gt</span> EDIT </code>         |
|     lt      | Less than                 | <code><span class="purple">lt</span> EDIT </code>         | <code><span class="purple">lt</span> EDIT </code>         |
|     le      | Less than or equal too    | <code><span class="purple">le</span> EDIT </code>         | <code><span class="purple">le</span> EDIT </code>         |
|     ge      | Greater than or equal too | <code><span class="purple">ge</span> EDIT </code>         | <code><span class="purple">ge</span> EDIT </code>         |
|     mi      | Minus                     | <code><span class="purple">mi</span> EDIT </code>         | <code><span class="purple">mi</span> EDIT </code>         |
|     pl      | Plus                      | <code><span class="purple">pl</span> EDIT </code>         | <code><span class="purple">pl</span> EDIT </code>         |
|     eq      | Equal                     | <code><span class="purple">eq</span> EDIT </code>         | <code><span class="purple">eq</span> EDIT </code>         |
|     ne      | Not Equal                 | <code><span class="purple">ne</span> EDIT </code>         | <code><span class="purple">ne</span> EDIT </code>         |
|      z      | Zero                      | <code><span class="purple">z</span> EDIT </code>          | <code><span class="purple">z</span> EDIT </code>          |
|     nz      | Not Zero                  | <code><span class="purple">nz</span> EDIT </code>         | <code><span class="purple">nz</span> EDIT </code>         |
|     cs      |                           | <code><span class="purple">cs</span> EDIT </code>         | <code><span class="purple">cs</span> EDIT </code>         |
|     cc      |                           | <code><span class="purple">cc</span> EDIT </code>         | <code><span class="purple">cc</span> EDIT </code>         |
|     vs      |                           | <code><span class="purple">vs</span> EDIT </code>         | <code><span class="purple">vs</span> EDIT </code>         |
|     vc      |                           | <code><span class="purple">vc</span> EDIT </code>         | <code><span class="purple">vc</span> EDIT </code>         |
|     hi      |                           | <code><span class="purple">hi</span> EDIT </code>         | <code><span class="purple">hi</span> EDIT </code>         |
|     lo      |                           | <code><span class="purple">lo</span> EDIT </code>         | <code><span class="purple">lo</span> EDIT </code>         |
|     hs      |                           | <code><span class="purple">hs</span> EDIT </code>         | <code><span class="purple">hs</span> EDIT </code>         |
|     ls      |                           | <code><span class="purple">ls</span> EDIT </code>         | <code><span class="purple">ls</span> EDIT </code>         |
|    macro    |                           | <code><span class="purple">macro</span> EDIT </code>      | <code><span class="purple">macro</span> EDIT </code>      |
|    mpop     |                           | <code><span class="purple">mpop</span> EDIT </code>       | <code><span class="purple">mpop</span> EDIT </code>       |
|    mpush    |                           | <code><span class="purple">mpush</span> EDIT </code>      | <code><span class="purple">mpush</span> EDIT </code>      |
|    mend     |                           | <code><span class="purple">mend</span> EDIT </code>       | <code><span class="purple">mend</span> EDIT </code>       |
|  continue   |                           | <code><span class="purple">continue</span> EDIT </code>   | <code><span class="purple">continue</span> EDIT </code>   |
|    wend     |                           | <code><span class="purple">wend</span> EDIT </code>       | <code><span class="purple">wend</span> EDIT </code>       |
|    until    |                           | <code><span class="purple">until</span> EDIT </code>      | <code><span class="purple">until</span> EDIT </code>      |
|    while    |                           | <code><span class="purple">while</span> EDIT </code>      | <code><span class="purple">while</span> EDIT </code>      |
|    save     |                           | <code><span class="purple">save</span> EDIT </code>       | <code><span class="purple">save</span> EDIT </code>       |
|   restore   |                           | <code><span class="purple">restore</span> EDIT </code>    | <code><span class="purple">restore</span> EDIT </code>    |
|   define    |                           | <code><span class="purple">define</span> EDIT </code>     | <code><span class="purple">define</span> EDIT </code>     |
|    stsp     |                           | <code><span class="purple">stsp</span> EDIT </code>       | <code><span class="purple">stsp</span> EDIT </code>       |
|    ldsp     |                           | <code><span class="purple">ldsp</span> EDIT </code>       | <code><span class="purple">ldsp</span> EDIT </code>       |
|    stays    |                           | <code><span class="purple">stays</span> EDIT </code>      | <code><span class="purple">stays</span> EDIT </code>      |
|    true     |                           | <code><span class="purple">true</span> EDIT </code>       | <code><span class="purple">true</span> EDIT </code>       |
|    break    |                           | <code><span class="purple">break</span> EDIT </code>      | <code><span class="purple">break</span> EDIT </code>      |
|     tst     | Test                      | <code><span class="purple">tst</span> <register> </code>  | <code><span class="purple">tst</span> r0 </code>          |
|     clr     |                           | <code><span class="purple">clr</span> EDIT </code>        | <code><span class="purple">clr</span> EDIT </code>        |
|     do      |                           | <code><span class="purple">do</span> EDIT </code>         | <code><span class="purple">do</span> EDIT </code>         |
|    then     |                           | <code><span class="purple">then</span> EDIT </code>       | <code><span class="purple">then</span> EDIT </code>       |
|   unique    |                           | <code><span class="purple">unique</span> EDIT </code>     | <code><span class="purple">unique</span> EDIT </code>     |
| first_item  |                           | <code><span class="purple">first_item</span> EDIT </code> | <code><span class="purple">first_item</span> EDIT </code> |
|    item     |                           | <code><span class="purple">item</span> EDIT </code>       | <code><span class="purple">item</span> EDIT </code>       |
|  last_item  |                           | <code><span class="purple">last_item</span> EDIT </code>  | <code><span class="purple">last_item</span> EDIT </code>  |
|     jmp     |                           | <code><span class="purple">jmp</span> EDIT </code>        | <code><span class="purple">jmp</span> EDIT </code>        |
|    jsrr     |                           | <code><span class="purple">jsrr</span> EDIT </code>       | <code><span class="purple">jsrr</span> EDIT </code>       |
|     shl     |                           | <code><span class="purple">shl</span> EDIT </code>        | <code><span class="purple">shl</span> EDIT </code>        |
|  banything  |                           | <code><span class="purple">banything</span> EDIT </code>  | <code><span class="purple">banything</span> EDIT </code>  |
|    bngt     |                           | <code><span class="purple">bngt</span> EDIT </code>       | <code><span class="purple">bngt</span> EDIT </code>       |
|    bnge     |                           | <code><span class="purple">bnge</span> EDIT </code>       | <code><span class="purple">bnge</span> EDIT </code>       |
|    bneq     |                           | <code><span class="purple">bneq</span> EDIT </code>       | <code><span class="purple">bneq</span> EDIT </code>       |
|    bnne     |                           | <code><span class="purple">bnne</span> EDIT </code>       | <code><span class="purple">bnne</span> EDIT </code>       |
|    bnlt     |                           | <code><span class="purple">bnlt</span> EDIT </code>       | <code><span class="purple">bnlt</span> EDIT </code>       |
|    bnle     |                           | <code><span class="purple">bnle</span> EDIT </code>       | <code><span class="purple">bnle</span> EDIT </code>       |
|    bnhi     |                           | <code><span class="purple">bnhi</span> EDIT </code>       | <code><span class="purple">bnhi</span> EDIT </code>       |
|    bnhs     |                           | <code><span class="purple">bnhs</span> EDIT </code>       | <code><span class="purple">bnhs</span> EDIT </code>       |
|    bncs     |                           | <code><span class="purple">bncs</span> EDIT </code>       | <code><span class="purple">bncs</span> EDIT </code>       |
|    bnlo     |                           | <code><span class="purple">bnlo</span> EDIT </code>       | <code><span class="purple">bnlo</span> EDIT </code>       |
|    bnls     |                           | <code><span class="purple">bnls</span> EDIT </code>       | <code><span class="purple">bnls</span> EDIT </code>       |
|    bncc     |                           | <code><span class="purple">bncc</span> EDIT </code>       | <code><span class="purple">bncc</span> EDIT </code>       |
|    bnmi     |                           | <code><span class="purple">bnmi</span> EDIT </code>       | <code><span class="purple">bnmi</span> EDIT </code>       |
|    bnpl     |                           | <code><span class="purple">bnpl</span> EDIT </code>       | <code><span class="purple">bnpl</span> EDIT </code>       |
|   bnfalse   |                           | <code><span class="purple">bnfalse</span> EDIT </code>    | <code><span class="purple">bnfalse</span> EDIT </code>    |
|   bntrue    |                           | <code><span class="purple">bntrue</span> EDIT </code>     | <code><span class="purple">bntrue</span> EDIT </code>     |
|    bnvs     |                           | <code><span class="purple">bnvs</span> EDIT </code>       | <code><span class="purple">bnvs</span> EDIT </code>       |
|    bnvc     |                           | <code><span class="purple">bnvc</span> EDIT </code>       | <code><span class="purple">bnvc</span> EDIT </code>       |
|    bnvs     |                           | <code><span class="purple">bnvs</span> EDIT </code>       | <code><span class="purple">bnvs</span> EDIT </code>       |
|   define    |                           | <code><span class="purple">define</span> EDIT </code>     | <code><span class="purple">define</span> EDIT </code>     |
|     ldv     |                           | <code><span class="purple">ldv</span> EDIT </code>        | <code><span class="purple">ldv</span> EDIT </code>        |
|     stv     |                           | <code><span class="purple">stv</span> EDIT </code>        | <code><span class="purple">stv</span> EDIT </code>        |
|     ei      |                           | <code><span class="purple">ei</span> EDIT </code>         | <code><span class="purple">ei</span> EDIT </code>         |
|     di      |                           | <code><span class="purple">di</span> EDIT </code>         | <code><span class="purple">di</span> EDIT </code>         |
|     rol     |                           | <code><span class="purple">rol</span> EDIT </code>        | <code><span class="purple">rol</span> EDIT </code>        |
|     nop     |                           | <code><span class="purple">nop</span> EDIT </code>        | <code><span class="purple">nop</span> EDIT </code>        |
