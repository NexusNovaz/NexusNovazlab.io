# Introduction

### This is a cheat sheet developed by Tyler for the CdM-8 processor built by Michael Walters and Alex Shafarenko at the University of Hertfordshire

This book will be updated as we find out more information on the instructions.

You can find the source on GitLab [**here**](https://gitlab.com/NexusNovaz/cdm-8-cheat-sheet).
Thats where you can either contribute to or notify us of errors.

<br><br><br>
Although not a requirement, if you're able to and would like to support what I'm trying to do
<script type='text/javascript' src='https://ko-fi.com/widgets/widget_2.js'></script><script type='text/javascript'>kofiwidget2.init('Support Me on Ko-fi', '#9028e0', 'U7U73QBER');kofiwidget2.draw();</script>
