# If Example

<pre><code><span class="green">asect</span> 0x00

	<span class="blue">ldi r0</span>, first

	<span class="blue">ld r0</span>, <span class="blue">r0</span>

	<span class="blue">ldi r1</span>, second

	<span class="blue">ld r1</span>, <span class="blue">r1</span>

	<span class="purple">if</span>
		<span class="blue">cmp r0</span>, <span class="blue">r1</span>
  <span class="purple">is</span> eq

  <span class="purple">fi</span>

	<span class="blue">ldi r3</span>, max

	<code><span class="blue">halt</span></code>

	first:  <span class="green">dc</span> -25
	second: <span class="green">dc</span> 13
	max:    <span class="green">ds</span> 1

<span class="green">end</span>
</code></pre>
