# Store Example

This will use the store to write to memory and display "Hello World!" on the I/O Port: OP_Dsip_16xChr

<pre><code><span class="green">asect</span> 0x00
	<span class="blue">ldi r0</span>, "H"
	<span class="blue">ldi r1</span>, 0xE0
	<span class="blue">st r1</span>, <span class="blue">r0</span>
	<span class="blue">ldi r2</span>, "e"
	<span class="blue">ldi r3</span>, 0xE1
	<span class="blue">st r3</span>, <span class="blue">r2</span>
	<span class="blue">ldi r2</span>, "l"
	<span class="blue">ldi r3</span>, 0xE2
	<span class="blue">st r3</span>, <span class="blue">r2</span>
	<span class="blue">ldi r2</span>, "l"
	<span class="blue">ldi r3</span>, 0xE3
	<span class="blue">st r3</span>, <span class="blue">r2</span>
	<span class="blue">ldi r2</span>, "o"
	<span class="blue">ldi r3</span>, 0xE4
	<span class="blue">st r3</span>, <span class="blue">r2</span>
	<span class="blue">ldi r2</span>, " "
	<span class="blue">ldi r3</span>, 0xE5
	<span class="blue">st r3</span>, <span class="blue">r2</span>
	<span class="blue">ldi r2</span>, "W"
	<span class="blue">ldi r3</span>, 0xE6
	<span class="blue">st r3</span>, <span class="blue">r2</span>
	<span class="blue">ldi r2</span>, "o"
	<span class="blue">ldi r3</span>, 0xE7
	<span class="blue">st r3</span>, <span class="blue">r2</span>
	<span class="blue">ldi r2</span>, "r"
	<span class="blue">ldi r3</span>, 0xE8
	<span class="blue">st r3</span>, <span class="blue">r2</span>
	<span class="blue">ldi r2</span>, "l"
	<span class="blue">ldi r3</span>, 0xE9
	<span class="blue">st r3</span>, <span class="blue">r2</span>
	<span class="blue">ldi r2</span>, "d"
	<span class="blue">ldi r3</span>, 0xEA
	<span class="blue">st r3</span>, <span class="blue">r2</span>
	<span class="blue">ldi r2</span>, "!"
	<span class="blue">ldi r3</span>, 0xEB
	<span class="blue">st r3</span>, <span class="blue">r2</span>
	<span class="blue">halt</span>
<span class="green">end</span>
</code></pre>

To try it out click "<span class="green">Run ►</span>" and open the file name "Store Example.asm", add an `OP_Disp_16xChr` I/O Port in the lower right, then select the speed you wish to run the code at in the top right, when you're ready click `Compile` then `Start`. You can resize the CoCoIDE window by dragging the middle bar to the right as the output is not needed.

<iframe height="700" width="100%" src="https://repl.it/@NexusNovaz/CoCoIDE?lite=true&outputonly=1" scrolling="no" frameborder="no" allowtransparency="true" allowfullscreen="true" sandbox="allow-forms allow-pointer-lock allow-popups allow-same-origin allow-scripts allow-modals"></iframe>
