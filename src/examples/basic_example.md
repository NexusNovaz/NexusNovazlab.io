# Basic Example

<pre><code><span class="green">asect</span> 0x00

	<span class="blue">ldi r0</span>, 0x05 <span class="comment"># Sets the register r0 to equal 5 can also be in binary: 0b00000101</span>

	<span class="blue">ldi r1</span>, 0x01 <span class="comment"># Sets the register r1 to equal 1 can also be in binary: 0b00000001</span>

	<span class="blue">add</span> <span class="blue">r0</span>, <span class="blue">r1</span> <span class="comment"># adds the registers r0 to r1 (r0+r1) and stores the value (6) in r1</span>

<span class="green">end</span>
</code></pre>

To try it out click "<span class="green">Run ►</span>" and open the file name "Example 1.asm", then select the speed you wish to run the code at in the top right, when you're ready click `Compile` then `Start`. You can resize the CoCoIDE window by dragging the middle bar to the right as the output is not needed.

<iframe height="700" width="100%" src="https://repl.it/@NexusNovaz/CoCoIDE?lite=true&outputonly=1" scrolling="no" frameborder="no" allowtransparency="true" allowfullscreen="true" sandbox="allow-forms allow-pointer-lock allow-popups allow-same-origin allow-scripts allow-modals"></iframe>
